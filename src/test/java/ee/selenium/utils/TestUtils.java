package ee.selenium.utils;

import java.util.Random;

public class TestUtils {

    public static String generateRandomString(int size) {
        String charactersSource = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            int generatedIndex = random.nextInt(charactersSource.length());
            char letter = charactersSource.charAt(generatedIndex);
            sb.append(letter);
        }
        return sb.toString();
    }
}
