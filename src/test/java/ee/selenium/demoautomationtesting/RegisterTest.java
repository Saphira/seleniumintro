package ee.selenium.demoautomationtesting;

// Generated by Selenium IDE

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterTest {
    private WebDriver driver;
    JavascriptExecutor js;
    WebElement dropdown;
    private WebDriverWait wait;

    @BeforeAll
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        js = (JavascriptExecutor) driver;
        wait = new WebDriverWait(driver, 30);

    }
    @AfterAll
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void register() {

        driver.get("http://demo.automationtesting.in/Register.html");
        driver.manage().window().setSize(new Dimension(1836, 1019));
        driver.findElement(By.xpath("//input[@type=\'text\']")).click();
        driver.findElement(By.xpath("//input[@placeholder=\'First Name\']")).sendKeys("test");
        driver.findElement(By.xpath("(//input[@type=\'text\'])[2]")).click();
        driver.findElement(By.xpath("(//input[@type=\'text\'])[2]")).sendKeys("test");
        driver.findElement(By.xpath("//form[@id=\'basicBootstrapForm\']/div[2]/div/textarea")).click();
        driver.findElement(By.xpath("//form[@id=\'basicBootstrapForm\']/div[2]/div/textarea")).sendKeys("123 Digital Lane");
        driver.findElement(By.xpath("//input[@type=\'tel\']")).sendKeys("5473925436");
        driver.findElement(By.id("countries"));
        dropdown.findElement(By.xpath("//option[. = 'United States']")).click();
        dropdown = driver.findElement(By.id("country"));
        dropdown.findElement(By.xpath("//option[. = 'United States of America']")).click();
        driver.findElement(By.xpath("//input[@type=\'email\']")).click();
        driver.findElement(By.xpath("//input[@type=\'email\']")).sendKeys("loal@gmail.com");
        driver.findElement(By.xpath("//form[@id=\'basicBootstrapForm\']/div[5]/div/label")).click();
        driver.findElement(By.name("radiooptions")).click();
        driver.findElement(By.id("checkbox1")).click();
        driver.findElement(By.id("firstpassword")).click();
        driver.findElement(By.id("firstpassword")).sendKeys("test");
        driver.findElement(By.id("secondpassword")).click();
        driver.findElement(By.id("secondpassword")).sendKeys("test");
        driver.findElement(By.id("submitbtn")).click();
        driver.findElement(By.id("yearbox")).click();
        dropdown.findElement(By.xpath("//option[. = '1927']")).click();
        driver.findElement(By.id("yearbox")).click();
        driver.findElement(By.xpath("(//select[@type=\'text\'])[4]")).click();
        dropdown.findElement(By.xpath("//option[. = 'February']")).click();
        driver.findElement(By.xpath("(//select[@type=\'text\'])[4]")).click();
        driver.findElement(By.id("daybox")).click();
        dropdown.findElement(By.xpath("//option[. = '12']")).click();
        driver.findElement(By.id("daybox")).click();
        driver.findElement(By.id("submitbtn")).click();
        driver.findElement(By.xpath("//form[@id=\'basicBootstrapForm\']/div[12]/div")).click();
        driver.findElement(By.id("firstpassword")).sendKeys("Test1234");
        driver.findElement(By.id("secondpassword")).click();
        driver.findElement(By.xpath("//form[@id=\'basicBootstrapForm\']/div[13]")).click();
        driver.findElement(By.id("secondpassword")).sendKeys("Test1234");
        driver.findElement(By.id("submitbtn")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[3]/div/div/div/div/div/div/div/div/div/div/div")));
        driver.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div/div/div/div/div")).click();
        driver.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div/div/div/div/div")).click();

    }
}

