package ee.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SDAPageTests {
    @Test
    public void firstTest(){
        //System.setProperty("webdriver.chrome.driver", "C:\\Workspace\\SDA\\Selenium\\drivers\\chromedrive\\chromedriver.exe");
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        //WebDriver driverFirefox = new FirefoxDriver();
        driver.get("https://sdacademy.dev");
        driver.manage().window().maximize();
        driver.close();
    }
}
