package ee.selenium.ecosia.tests;

import ee.selenium.ecosia.pages.EcosiaSearchPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(value = Lifecycle.PER_CLASS)
public class JunitDataDrivenEcosiaTests {
    private static WebDriver driver;
    private static EcosiaSearchPage objEcosiaSearchPage;

    @BeforeAll
    static void setup() throws URISyntaxException {

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.ecosia.org/");
    }

    @AfterAll
    static void tearDown() {
        driver.quit();
    }

    @ParameterizedTest
    @ValueSource(strings = {"seleniumhq.org", "junit.org"})
    public void test_EcosiaSearch(String text) throws InterruptedException {
        objEcosiaSearchPage = new EcosiaSearchPage(driver);

        objEcosiaSearchPage.setQuery(text);
        objEcosiaSearchPage.search();

        List<WebElement> linkElements = objEcosiaSearchPage.getResults();

        Optional<WebElement> result = linkElements.stream()
                .filter(a -> a.getText().toLowerCase().contains(text))
                .findAny();

        assertTrue(result.isPresent());

    }
}

