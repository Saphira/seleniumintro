package ee.selenium.ohrm.tests;

import ee.selenium.ohrm.utils.Steps;
import org.junit.jupiter.api.*;

import static ee.selenium.ohrm.utils.TestCommons.*;
import static org.junit.jupiter.api.Assertions.*;

public class LoginTests extends TestTemplate{

    @BeforeAll
    public void setUp() { set(); }

    @AfterAll
    public void tearDown() {
        driver.quit();
    }

    @AfterEach
    public void closeWindow(){
        log.info("Close window");
        driver.close();
    }

    @Test
    @DisplayName("Login is successful with correct User and Password")
    public void loginSuccessful(){
        log.info("Login");
        Steps.login(driver, wait, js, USER, PASSWORD);
        assertTrue(driver.getTitle().equals(TITLE_HOME));

    }

    @Test
    @DisplayName("Login fails with incorrect User and correct Password")
    public void loginFailsIncorrectUser(){
        Steps.login(driver, wait, js, "incorrectUser", PASSWORD);
        assertTrue(driver.getCurrentUrl().equals(BASE_URL));

    }

    @Test
    @DisplayName("Login fails with correct User and incorrect Password")
    public void loginFailsIncorrectPassword(){
        Steps.login(driver, wait, js, USER, "incorrectPassword");
        assertTrue(driver.getCurrentUrl().equals(BASE_URL));

    }


}
