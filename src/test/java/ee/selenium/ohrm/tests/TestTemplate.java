package ee.selenium.ohrm.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

@TestInstance(value = TestInstance.Lifecycle.PER_CLASS )
public class TestTemplate {
    protected static WebDriver driver;
    protected static JavascriptExecutor js;
    protected static Logger log;
    protected static WebDriverWait wait;

    public void set(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        js = (JavascriptExecutor) driver;
        wait = new WebDriverWait(driver, 30);
        log = Logger.getLogger(TestTemplate.class);
    }

}
