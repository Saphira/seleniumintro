package ee.selenium.ohrm.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static ee.selenium.ohrm.utils.TestCommons.*;

public class Steps {
    public static void login(WebDriver driver, WebDriverWait wait, JavascriptExecutor js, String user, String password) {
        driver.get(BASE_URL);
        driver.manage().window().maximize();
        driver.findElement(By.cssSelector("#divUsername > .form-hint")).click();
        driver.findElement(By.id("txtUsername")).sendKeys(user);
        driver.findElement(By.id("txtPassword")).click();
        driver.findElement(By.id("txtPassword")).sendKeys(password);
        driver.findElement(By.id("btnLogin")).click();

    }
}