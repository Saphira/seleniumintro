package ee.selenium.ecomerce;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.jupiter.api.Assertions.assertTrue;

import static ee.selenium.ecomerce.TestCommons.*;

public class EcommerceTests {
    private static WebDriver driver;
    private static JavascriptExecutor js;
    private static Logger log;
    private static WebDriverWait wait;

    @BeforeAll
    static void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        js = (JavascriptExecutor) driver;
        wait = new WebDriverWait(driver, 30);
        log = Logger.getLogger(EcommerceTests.class);
    }

    @AfterAll
    static void tearDown() {
        driver.quit();
    }

    @BeforeEach
    void logIn() {
        driver.get(BASE_URL);
        driver.manage().window().maximize();
        driver.findElement(By.className("login")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("email")));
        driver.findElement(By.id("email")).click();
        driver.findElement(By.id("email")).sendKeys(EMAIL);
        driver.findElement(By.id("passwd")).click();
        driver.findElement(By.id("passwd")).sendKeys(PASSWORD);
        driver.findElement(By.id("SubmitLogin")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(),'My account')]")));
    }

    @Test
    public void buyItems() {
        log.info("Click Dresses");
        driver.findElement(By.xpath("//header/div[3]/div[1]/div[1]/div[6]/ul[1]/li[2]/a[1]")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[contains(text(),'Dresses')]")));
        driver.findElement(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/ul[1]/li[2]/a[1]")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[2]/h1[1]/span[1]")));
        driver.findElement(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[2]/div[2]/div[1]/ul[1]/li[3]/a[1]")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[2]/ul[1]/li[1]/div[1]/div[1]/div[3]/div[1]/div[2]/a[1]")));
        driver.findElement(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[2]/ul[1]/li[1]/div[1]/div[1]/div[3]/div[1]/div[2]/a[1]")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//header/div[3]/div[1]/div[1]/div[4]/div[1]/div[2]/div[4]/a[1]/span[1]")));
        driver.findElement(By.xpath("//header/div[3]/div[1]/div[1]/div[4]/div[1]/div[2]/div[4]/a[1]/span[1]")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[1]/p[2]/a[1]/span[1]")));
        driver.findElement(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[1]/p[2]/a[1]")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[1]/form[1]/p[1]/button[1]/span[1]")));
        driver.findElement(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[1]/form[1]/p[1]/button[1]/span[1]")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='cgv']")));
        WebElement checkbox = driver.findElement(By.xpath("//input[@id='cgv']"));
        js.executeScript("arguments[0].click();", checkbox);
        driver.findElement(By.name("processCarrier")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/p[1]/a[1]")));
        driver.findElement(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/p[1]/a[1]")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[1]/form[1]/p[1]/button[1]")));
        driver.findElement(By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[1]/form[1]/p[1]/button[1]")).click();

        assertTrue(driver.findElement(By.xpath("//h1[contains(text(),'Order confirmation')]")).isDisplayed());
    }
}
